# Projeto: 

Sobre nóticias e bolsa de valores. 

## Equipe: 

Sotck&News. 

## Descrição: 

Busca-se compreender como determinados termos/assuntos de notícias podem influenciar na bolsa de valores de São Paulo. Através de uma base de dados de um site de notícias relevante, busca-se compreender como os termos e assuntos tratados estavam relacionados com a perspectiva dos investidores da bolsa de valores, no sentido que se achassem que determinado assunto poderia impactar positivamente, investiriam e aplicariam mais seu capital guardado, e quando achassem que a notícia teria impacto negativo, poderiam liquidar e guardar o capital para futuros investimentos. 


## Membros:

Mateus Pereira Gomes, 1538438, mateuspereiragomes,  mateuspereiragomes@gmail.com, Sistemas de Informação, UTFPR

